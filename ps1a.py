portion_down_payment = 0.25
current_savings = 0
r = 0.04 / 12 + 1
annual_salary = int(input())
mounthly_salary = annual_salary / 12
portion_saved = 0
total_cost = 1000000
semi_annual_raise = 0.07
NoM = 0
down_payment = total_cost*portion_down_payment
while NoM != 36:
    NoM = 0
    current_savings = 0
    mounthly_salary = annual_salary / 12
    portion_saved += 0.0001
    while current_savings < down_payment:
        current_savings *= r
        current_savings += (mounthly_salary*portion_saved)
        NoM += 1
        if NoM % 6 == 0:
            mounthly_salary *= (1+semi_annual_raise)
if portion_saved < 1:
    print(round(portion_saved*10000)/10000)
else:
    print("Impossible to pay the down payment in three years")

